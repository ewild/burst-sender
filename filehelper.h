#pragma once
/*
 * Copyright 2022 sysmocom - s.f.m.c. GmbH
 *
 * Author: Eric Wild <ewild@sysmocom.de>
 *
 * SPDX-License-Identifier: AGPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * See the COPYING file in the main directory for details.
 */

#include <chrono>
#include <climits>
#include <complex>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <utility>
#include <vector>
#include <complex>

#include <dirent.h>

using namespace std;

const int SAMPLE_SCALE_FACTOR = 1;
template <typename T> struct readf_res {
	using value_type = T;
	const unsigned int num_samp;
	std::shared_ptr<T> buf;
	explicit readf_res(unsigned int num_samples)
		: num_samp(num_samples), buf(std::shared_ptr<T>(new T[num_samples], [](const T *ptr) { delete[] ptr; }))
	{
	}
	explicit readf_res(unsigned int num_samples, std::shared_ptr<T> ptr)
		: num_samp(num_samples), buf(std::move(ptr), [](const T *ptr) { delete[] ptr; })
	{
	}
	explicit readf_res(unsigned int num_samples, T *ptr)
		: num_samp(num_samples), buf(ptr, [](const T *ptr) { delete[] ptr; })
	{
	}
	readf_res() = delete;
	readf_res(const readf_res &) = delete;
	readf_res &operator=(const readf_res &) = delete;
	~readf_res() = default;
	unsigned int size_in_byte()
	{
		return num_samp * sizeof(T);
	}
	const char *getbytes()
	{
		return (char *)buf.get();
	}
};
template <typename T> readf_res<T> *readf(const char *path)
{
	streampos begin, end;
	ifstream myfile(path, ios::binary);
	begin = myfile.tellg();
	myfile.seekg(0, ios::end);
	end = myfile.tellg();
	auto len_in_byte = end - begin;
	auto len_in_sps = len_in_byte / sizeof(T);
	T *T_buf = new T[len_in_sps];

	myfile.seekg(0, ios::beg);
	myfile.read((char *)T_buf, len_in_byte);
	myfile.close();
	// cout << "size is: " << (len_in_byte) << " bytes, " << len_in_sps << " samples.\n";
	return new readf_res<T>{ (unsigned int)len_in_sps, T_buf };
}

std::vector<std::string> list_dir(std::string path)
{
	std::vector<std::string> files;
	struct dirent *entry = nullptr;
	char resolved[PATH_MAX];
	realpath(path.c_str(), resolved);
	DIR *dir = opendir(path.c_str());
	if (dir == nullptr) {
		return {};
	}

	while ((entry = readdir(dir)) != nullptr) {
		if (entry->d_type == DT_REG) {
			files.emplace_back(std::string(resolved) + "/" + entry->d_name);
		}
	}

	closedir(dir);
	return files;
}

std::vector<std::string> filter_entries(std::vector<std::string> e, std::string file_ending)
{
	std::vector<std::string> filtered = {};
	for (auto s : e) {
		auto mm = mismatch(file_ending.rbegin(), file_ending.rend(), s.rbegin()).first != file_ending.rend();
		if (!mm)
			filtered.emplace_back(s);
	}
	return filtered;
}

namespace cvt_internal
{

template <typename SRC_T, typename ST>
void convert_and_scale_i(float *dst, const SRC_T *src, unsigned int src_len, ST scale)
{
	for (unsigned int i = 0; i < src_len; i++)
		dst[i] = static_cast<float>(src[i]) * scale;
}

template <typename DST_T, typename ST>
void convert_and_scale_i(DST_T *dst, const float *src, unsigned int src_len, ST scale)
{
	for (unsigned int i = 0; i < src_len; i++)
		dst[i] = static_cast<DST_T>(src[i] * scale);
}

template <typename ST> void convert_and_scale_i(float *dst, const float *src, unsigned int src_len, ST scale)
{
	for (unsigned int i = 0; i < src_len; i++)
		dst[i] = src[i] * scale;
}

template <typename T> struct is_complex : std::false_type {
	using baset = T;
};

template <typename T> struct is_complex<std::complex<T>> : std::true_type {
	using baset = typename std::complex<T>::value_type;
};


} // namespace cvt_internal

template <typename DST_T, typename SRC_T, typename ST>
void convert_and_scale(DST_T *dst, const SRC_T *src, unsigned int src_len, ST scale)
{
	using vd = typename cvt_internal::is_complex<DST_T>::baset;
	using vs = typename cvt_internal::is_complex<SRC_T>::baset;
	return cvt_internal::convert_and_scale_i((vd *)dst, (vs *)src, src_len, scale);
}