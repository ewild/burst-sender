#pragma once
/*
 * Copyright 2022 sysmocom - s.f.m.c. GmbH
 *
 * Author: Eric Wild <ewild@sysmocom.de>
 *
 * SPDX-License-Identifier: AGPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * See the COPYING file in the main directory for details.
 */

#include <cstdint>
#include <iostream>
#include <vector>

template <typename A> bool parsec(std::vector<std::string> &v, A &itr, std::string arg, bool *rv)
{
	if (*itr == arg) {
		*rv = true;
		return true;
	}
	return false;
}

template <typename A, typename B, typename C>
bool parsec(std::vector<std::string> &v, A &itr, std::string arg, B f, C *rv)
{
	if (*itr == arg) {
		itr++;
		if (itr != v.end()) {
			*rv = f(itr->c_str());
			return true;
		}
	}
	return false;
}
template <typename A> bool parsec(std::vector<std::string> &v, A &itr, std::string arg, int64_t scale, int64_t *rv)
{
	return parsec(
		v, itr, arg, [scale](const char *v) -> decltype(scale) { return atoll(v) * scale; }, rv);
}
template <typename A> bool parsec(std::vector<std::string> &v, A &itr, std::string arg, int scale, int *rv)
{
	return parsec(
		v, itr, arg, [scale](const char *v) -> decltype(scale) { return atoi(v) * scale; }, rv);
}
template <typename A> bool parsec(std::vector<std::string> &v, A &itr, std::string arg, int scale, unsigned int *rv)
{
	return parsec(
		v, itr, arg, [scale](const char *v) -> decltype(scale) { return atoi(v) * scale; }, rv);
}
template <typename A> bool parsec(std::vector<std::string> &v, A &itr, std::string arg, float scale, float *rv)
{
	return parsec(
		v, itr, arg, [scale](const char *v) -> decltype(scale) { return decltype(scale)(atof(v) * scale); },
		rv);
}
template <typename A> bool parsec(std::vector<std::string> &v, A &itr, std::string arg, std::string *rv)
{
	return parsec(
		v, itr, arg, [](const char *v) -> std::string { return { v }; }, rv);
}
